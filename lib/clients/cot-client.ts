import { Observable } from 'rxjs/Observable';
import axios, { AxiosResponse, AxiosInstance, AxiosRequestConfig } from 'axios';

import { SourcesClient } from './sources-client';
import { LocationsClient } from './locations-client';
import { InternalUtils as _utils } from '../internal-utils';
import { Type, Source, ExpandedSource, Metric, TemporalPage, TemporalPageLink } from '../types';
import { DataClient } from './data-client';
import { LayerClient } from './layer-client';

export class CotClient {
    protected host: string;
    protected ax: AxiosInstance;
    private lastReceivedTime: number = -1;

    /**
     * Creates a new CotClient, with optional Basic Auth credentials
     * @param host The host url of the new City of things client. (e.g. http://www.cot-backend.com/api);
     * @param credentials {CotClientCredentials=} Optional credentials object
     */
    constructor(host: string, credentials?: CotClientCredentials) {
        if (host.endsWith('/')) {
            host = host.substr(0, host.length - 1);
        }
        this.host = host;
        if (credentials) {
            let cfg = { auth: credentials };
            this.ax = axios.create(cfg);
        }
        else {
            this.ax = axios.create();
        }
    }

    // /**
    //  * Returns all available types as an array of *Type* objects.
    //  * @returns An Observable containg a *Type* array.
    //  */
    // getTypes(): Observable<Type[]> {
    //     let url = this.host + '/types';
    //     return Observable.fromPromise(this.ax.get(url)).pluck('data');
    // }

    // /**
    //  * Returns the metadata of a type as a Type object.
    //  * @param typeId {string} The id of the Type to get the metadata from.
    //  * @returns An Observable containing a *Type* object.
    //  */
    // getType(typeId: string): Observable<Type> {
    //     let url = this.host + '/types/' + typeId;
    //     return Observable.fromPromise(this.ax.get(url)).pluck('data');
    // }

    /**
     * Returns all available sources as an array of Source objects
     * @returns An Observable containing a Source array.
     */
    getSources(): Observable<Source[]> {
        let url = this.host + '/sources';
        return Observable.fromPromise(this.ax.get(url)).pluck('data');
    }

    /**
     * Returns the metadata of a source as a Source object.
     * @param sourceId {string} The id of the Source to get the metadata from.
     * @param expanded {boolean} Whether to expand the typeRefs or metrics from ids to full Types Metrics (default: false)
     * @returns An Observable containing a *Source* object.
     */
    getSource(sourceId: string, expanded: boolean = false): Observable<Source | ExpandedSource> {
        let url = this.host + '/sources/' + sourceId;
        if (expanded) {
            url += '/expanded';
        }
        return Observable.fromPromise(this.ax.get(url)).pluck('data');
    }

    /**
     * Lists all Metric definitions.
     * @returns An Observable cotaining an array of *Metric* objects.
     */
    getMetrics(): Observable<Metric[]> {
        let url = this.host + '/metrics';
        return Observable.fromPromise(this.ax.get(url)).pluck('data');
    }
    
    /**
     * Returns the definition of a Metric.
     * @param The id of a metric
     * @returns An Observable containing a *Metric* object.
     */
    getMetric(metricId: string): Observable<Metric> {
        let url = this.host + '/metrics/' + metricId;
        return Observable.fromPromise(this.ax.get(url)).pluck('data');
    }

    /**
     * Returns the ids of the city data layers.
     */
    getCityIds(): Observable<string[]> {
        let url = this.host + '/layers/';
        return Observable.fromPromise(this.ax.get(url)).pluck('data');
    }

    /* Client creation methods */

    /**
     * Returns a MultiClient that internally iterates over all given sourceIds when requesting data or stats.
     * @param sourceIds {string[]} The sourceIds to do simultaneous queries on.
     * @returns A new SourcesClient object.
     */
    withSources(...sourceIds: string[]): DataClient {
        return new SourcesClient(this.host, this.ax, ...sourceIds);
    }

    /**
     * Returns a MultiClient that internally iterates over all given geohashes when requesting data or stats.
     * @param geohashes {string[]} The geohashes to do simultaneous queries in.
     * @returns A new LocationsClient object.
     */
    withLocations(...geohashes: string[]): DataClient {
        return new LocationsClient(this.host, this.ax, ...geohashes);
    }

    /**
     * Returns a LayerClient that provides an entry point to layer data per metric for the given cityId
     * @param cityId An id that signifies both the city location and the data granularity
     */
    withCityId(cityId: string): LayerClient {
        return new LayerClient(this.host, this.ax, cityId);
    }
}

export interface CotClientCredentials {
    username: string;
    password: string;
};