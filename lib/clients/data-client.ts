import { Observable } from 'rxjs/Observable';
import axios from 'axios';
import { AxiosResponse, AxiosInstance } from 'axios';

import { Util } from '../util';
import { InternalUtils as _utils } from '../internal-utils';
import { TemporalPage, TemporalPageLink, Data, NamedData, BatchedData } from '../types';

var URLSearchParams = require('url-search-params');

export abstract class DataClient {
    protected host: string;
    protected ax: AxiosInstance;
    constructor(host: string, axiosInstance: AxiosInstance) {
        this.host = host;
        this.ax = axiosInstance || axios.create();
    }

    /**
     * The item collection over which the MultiClient iterates (sourceIds or geohashes).
     */
    protected abstract getMultiItems(): string[];

    /**
     * The path portion of the api, starting with '/' ending with '/events'.
     * @param item The injected item of the item collection returned by getMultiItems()
     */
    protected abstract getBaseApiPath(item: string): string;

    private generateLatestUrls(metric: string): string[] {
        return this.getMultiItems().map(item => this.host + this.getBaseApiPath(item) + '/' + metric + '/events/latest');
    }

    private generateEventUrls(metric: string, queryStr: string): string[] {
        return this.getMultiItems().map(item => this.host + this.getBaseApiPath(item) + '/' + metric + '/events' + ((queryStr && queryStr.length > 0) ? '?' + queryStr : ''));
    }

    private generateStatsUrls(metric: string, type: 'unit' | 'batch', timeStr: string): string[] {
        return this.getMultiItems().map(item => this.host + this.getBaseApiPath(item) + '/' + metric + '/stats/' + type + '/' + timeStr);
    }

    /**
     * Methods for retrieving data of a particual metric
     * @param metric metric name
     */
    getData(metric: string): IData {
        let that = this;
        return {
            latest(): Observable<TemporalPage> {
                return Observable.merge(...that.generateLatestUrls(metric).map(url => _utils.getAsTemporalPage(url, that.ax)));
            },
            poll(period: number, fromTS?: number): Observable<TemporalPage> {
                let last: number;
                let queryStr = new URLSearchParams();
                if (fromTS) {
                    queryStr.append('from', fromTS.toString());
                }
                return Observable.merge(...that.generateEventUrls(metric, queryStr.toString())
                    .map(url => _utils.getAsTemporalPage(url, that.ax)
                        .delay(period)
                        .expand(tpage => {
                            if (!!tpage.link.next) {
                                let t = _utils.getLastTimestamp(tpage);
                                last = t == null ? last : t;
                                // console.log(last);
                                return _utils.getAsTemporalPage(that.host + tpage.link.next, that.ax, last).delay(period);
                            }
                            else {
                                return Observable.empty();
                            }
                        })
                    ));
            },
            range(fromTS: number, toTS: number): Observable<TemporalPage> {
                let queryStr = new URLSearchParams();
                queryStr.append('from', fromTS.toString());
                queryStr.append('to', toTS.toString());
                return Observable
                    .merge(...that.generateEventUrls(metric, queryStr.toString()).map(url => _utils.getCompleteInterval(that.host, url, that.ax)))
                    .filter(page => Object.keys(page.data).length > 0);
                    // .pluck('data');
            }
        };
    };

    /**
     * Methods for retrieving statistics of a particular metric
     */
    getStats(metric: string): IStats {
        let that = this;
        return {
            getUnit(): IStatsUnit {
                return {
                    getHour(hourTS: number): Observable<TemporalPage> {
                        return Observable.merge(...that.generateStatsUrls(metric, 'unit', Util.Time.getHourStr(hourTS)).map(url => _utils.getAsTemporalPage(url, that.ax)));
                    },
                    getHourRange(fromTS: number, toTS: number): Observable<TemporalPage> {
                        return that.getStats(metric).getUnit().getHour(fromTS).expand(tpage => {
                            let url = tpage.link.next;
                            if (tpage.link.self.endsWith(Util.Time.getHourStr(toTS))) {
                                return Observable.empty();
                            } else {
                                return _utils.getAsTemporalPage(that.host + url, that.ax);
                            }
                        });
                    },
                    getHourRecap(fromTS: number, toTS: number): Observable<Data> {
                        return that.getStats(metric).getUnit().getHourRange(fromTS, toTS)
                            .filter(o => !o.data.nodata)
                            .map(o => o.data as Data)
                            .reduce((acc, curr) => _utils.mergeStatsData(acc, curr));
                    },
                    getHourRecapOf(...hourTS: number[]): Observable<Data> {
                        return Observable.from(hourTS).flatMap(ts => that.getStats(metric).getUnit().getHour(ts))
                            .filter(o => !o.data.nodata)
                            .map(o => o.data as Data)
                            .reduce((acc, curr) => _utils.mergeStatsData(acc, curr));
                    },
                    getDay(dayTS: number): Observable<TemporalPage> {
                        return Observable.merge(...that.generateStatsUrls(metric, 'unit', Util.Time.getDayStr(dayTS)).map(url => _utils.getAsTemporalPage(url, that.ax)));
                    },
                    getDayRange(fromTS: number, toTS: number): Observable<TemporalPage> {
                        return that.getStats(metric).getUnit().getDay(fromTS).expand(tpage => {
                            let url = tpage.link.next;
                            if (tpage.link.self.endsWith(Util.Time.getDayStr(toTS))) {
                                return Observable.empty();
                            } else {
                                return _utils.getAsTemporalPage(that.host + url, that.ax);
                            }
                        });
                    },
                    getDayRecap(fromTS: number, toTS: number): Observable<Data> {
                        return that.getStats(metric).getUnit().getDayRange(fromTS, toTS)
                            .filter(o => !o.data.nodata)
                            .map(o => o.data as Data)
                            .reduce((acc, curr) => _utils.mergeStatsData(acc, curr));
                    },
                    getDayRecapOf(...dayTS: number[]): Observable<Data> {
                        return Observable.from(dayTS).flatMap(ts => that.getStats(metric).getUnit().getDay(ts))
                            .filter(o => !o.data.nodata)
                            .map(o => o.data as Data)
                            .reduce((acc, curr) => _utils.mergeStatsData(acc, curr));
                    },
                };
            },
            getBatch(): IStatsBatch {
                return {
                    getDay(dayTS: number): Observable<TemporalPage> {
                        return Observable.merge(...that.generateStatsUrls(metric, 'batch', Util.Time.getDayStr(dayTS))
                            .map(url => _utils.getAsTemporalPage(url, that.ax).filter(res => Object.keys(res.data).length > 0)));
                    },
                    getDayRange(fromTS: number, toTS: number): Observable<TemporalPage> {
                        return that.getStats(metric).getBatch().getDay(fromTS).expand(tpage => {
                            let url = tpage.link.next;
                            if (tpage.link.self.endsWith(Util.Time.getDayStr(toTS))) {
                                return Observable.empty();
                            } else {
                                return _utils.getAsTemporalPage(that.host + url, that.ax);
                            }
                        });
                    },
                    getDayRecap(fromTS: number, toTS: number): Observable<BatchedData> {
                        return that.getStats(metric).getBatch().getDayRange(fromTS, toTS)
                            .map(o => o.data as BatchedData)
                            .reduce((acc, curr) => _utils.mergeStatsBatchedData(acc, curr));
                    },
                    getDayRecapOf(...dayTS: number[]): Observable<BatchedData> {
                        return Observable.from(dayTS).flatMap(ts => that.getStats(metric).getBatch().getDay(ts))
                            .map(o => o.data as BatchedData)
                            .reduce((acc, curr) => _utils.mergeStatsBatchedData(acc, curr));
                    },
                }
            }
        };
    }
}

/**
 * Methods for retrieving data
 */
export interface IData {
    /**
     * Returns the latest data of each item (location or source).
     */
    latest: () => Observable<TemporalPage>;

    /**
     * Poll for the latest data of each item (location or source). Every update will be sent through, 
     * hence it is mandatory to check the data for the key.
     * @param period {number} The period in milliseconds of the polling.
     * @param fromTS {number=} The UNIX UTC timestamp from which to start polling. If not present the last data will be polled.
     * @see Util.Time.timestampOf to easily create a UTC timestamp
     */
    poll: (period: number, fromTS?: number) => Observable<TemporalPage>;

    /**
     * Returns data for each item (location or source) within the assigned areas as a single array of TemporalPageData objects.
     * @param fromTS {number} Timestamp (UNIX UTC) from which to start the requested range
     * @param toTS {number} Timestamp (UNIX UTC) until which to query
     * @returns An Observable containing an array of objects.
     * @see Util.Time.timestampOf to easily create a UTC timestamp
     */
    range: (fromTS: number, toTS: number) => Observable<TemporalPage>;
}

/**
 * Methods for retrieving statistics
 */
export interface IStats {
    /**
     * Methods for retrieving a single statistics unit (of an hour/day)
     */
    getUnit: () => IStatsUnit;
    /**
     * Methods for retrieving a group of statistics (of a day)
     */
    getBatch: () => IStatsBatch;
}

export interface IStatsUnit {
    /**
     * Returns statistics for all measured values in one hour.
     * @param hourTS The UTC timestamp of a single hour
     * @see Util.Time.timestampOf to easily create a UTC timestamp
     */
    getHour: (hourTS: number) => Observable<TemporalPage>;

    /**
     * Returns statistics for all measured values per hour in a range.
     * @param fromTS The UTC timestamp of a single hour to start the range from 
     * @param toTS The UTC timestamp of a single hour to end the range with 
     * @see Util.Time.timestampOf to easily create a UTC timestamp
     */
    getHourRange: (fromTS: number, toTS: number) => Observable<TemporalPage>;

    /**
     * Get a single statistics hour unit as a recap representing the requested range.
     * @param fromTS The UTC timestamp of a single hour to start the range from 
     * @param toTS The UTC timestamp of a single hour to end the range with 
     * @see Util.Time.timestampOf to easily create a UTC timestamp
     */
    getHourRecap: (fromTS: number, toTS: number) => Observable<Data>;

    /**
     * Get a single statistics hour unit as a recap representing the requested hours.
     * @param hourTS All timestamps representing the hours to get a recap from
     * @see Util.Time.timestampOf to easily create a UTC timestamp
     */
    getHourRecapOf: (...hourTS: number[]) => Observable<Data>;

    /**
     * Returns statistics for all measured values in one day.
     * @param dayTS The UTC timestamp of a single day
     * @see Util.Time.timestampOf to easily create a UTC timestamp
     */
    getDay: (dayTS: number) => Observable<TemporalPage>;

    /**
     * Returns statistics for all measured values per day in an range.
     * @param fromTS The UTC timestamp of a single day to start the range from 
     * @param toTS The UTC timestamp of a single day to end the range with 
     * @see Util.Time.timestampOf to easily create a UTC timestamp
     */
    getDayRange: (fromTS: number, toTS: number) => Observable<TemporalPage>;

    /**
     * Get a single statistics day unit as a recap representing the requested range.
     * @param fromTS The UTC timestamp of a single day to start the range from 
     * @param toTS The UTC timestamp of a single day to end the range with 
     * @see Util.Time.timestampOf to easily create a UTC timestamp
     */
    getDayRecap: (fromTS: number, toTS: number) => Observable<Data>;

    /**
     * Get a single statistics day unit as a recap representing the requested days.
     * @param dayTS All timestamps representing the days to get a recap from
     * @see Util.Time.timestampOf to easily create a UTC timestamp
     */
    getDayRecapOf: (...dayTS: number[]) => Observable<Data>


}

export interface IStatsBatch {
    /**
     * Returns a disgest of statistics for all measured values of a single day. The batch contains an overview of multiple bins.
     * @param dayTS The UTC timestamp of a single day 
     * @see Util.Time.timestampOf to easily create a UTC timestamp
     */
    getDay: (dayTS: number) => Observable<TemporalPage>;

    /**
     * Returns a batch of statistics for all measured values per day in an range.
     * @param fromTS The UTC timestamp of a single day to start the range from 
     * @param toTS The UTC timestamp of a single day to end the range with 
     * @see Util.Time.timestampOf to easily create a UTC timestamp
     */
    getDayRange: (fromTS: number, toTS: number) => Observable<TemporalPage>;

    /**
     * Get a single statistics day batch as a recap representing the requested range
     * @param fromTS The UTC timestamp of a single day to start the range from 
     * @param toTS The UTC timestamp of a single day to end the range with 
     */
    getDayRecap: (fromTS: number, toTS: number) => Observable<BatchedData>;

    /**
    * Get a single statistics day batch as a recap representing the requested days.
    * @param dayTS All timestamps representing the days to get a recap from
    * @see Util.Time.timestampOf to easily create a UTC timestamp
    */
    getDayRecapOf: (...dayTS: number[]) => Observable<BatchedData>;


}