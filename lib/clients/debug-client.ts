import { Observable } from 'rxjs/Rx';
import { CotClient, CotClientCredentials } from './cot-client';
import { InternalUtils as _utils } from '../internal-utils';
import { TemporalPage, TemporalPageLink } from '../types';

export class DebugClient extends CotClient {

    constructor(host: string, credentials?: CotClientCredentials) {
        super(host, credentials);
    }

    /**
     * Returns only a single TemporalPage for the request.
     * @param sourceId {string} The sourceId of the *Source* to poll from.
     * @param from {number=} The UNIX UTC timestamp from which to do the request. If not present the last data page will be requested.
     */
    getTemporalPage(sourceId: string, from?: number): Observable<TemporalPage> {
        let query = '';
        if (from) {
            query += '?from=' + from;
        }
        let url = this.host + '/sources/' + sourceId + '/events' + query;
        return _utils.getAsTemporalPage(url, this.ax);
    }

    /**
     * Returns only a single TemporalPage for the request.
     * @param geohash {string} The geohash defining the location.
     * @param typeId {string} The typeId of the *Type* of data to get.
     * @param from {number=} The UNIX UTC timestamp from which to do the request. If not present the last data page will be requested.
     */
    getTemporalPageIn(geohash: string, typeId: string, from?: number): Observable<TemporalPage> {
        let query = '';
        if (from) {
            query += '?from=' + from;
        }
        let url = this.host + '/locations/' + geohash + '/' + typeId + '/events' + query;
        return _utils.getAsTemporalPage(url, this.ax);
    }

    /**
     * Tries to parse a request from a complete url to a TemporalPage
     * @param url The complete url (including origin part)
     */
    getTemporalPageForUrl(url: string): Observable<TemporalPage> {
        return _utils.getAsTemporalPage(url, this.ax);
    }

    /**
     * Attches the given link to the host and returns it as a TemporalPage
     * @param link {string} The link to follow (eg. /source/id1/events?from=123456)
     */
    getLink(link: string): Observable<TemporalPage> {
        let url = this.host + link;
        return _utils.getAsTemporalPage(url, this.ax);
    }
}