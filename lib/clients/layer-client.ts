import { Observable } from 'rxjs/Observable';
import axios from 'axios';
import { AxiosResponse, AxiosInstance } from 'axios';

import { Util } from '../util';
import { NamedData, TemporalPage } from '../types';
import { url as _url, InternalUtils as _utils } from '../internal-utils';

export class LayerClient {
    private host: string;
    private ax: AxiosInstance;
    private cityId: string;

    constructor(host: string, axiosInstance: AxiosInstance, cityId: string) {
        this.host = host;
        this.ax = axiosInstance || axios.create();
        this.cityId = cityId;
    }

    /**
     * Get layer data of the requested metric.
     * @param metric Metric of the data for the layer
     */
    getLayer(metric: string): IDataLayer {
        let that = this;
        return {
            getHour(hourTS: number): Observable<TemporalPage> {
                const hourStr = Util.Time.getHourStr(hourTS)
                const url = _url(that.host, 'layers', that.cityId, metric, hourStr);
                return _utils.getAsTemporalPage(url, that.ax);
            },
            getHourRange(fromTS: number, toTS: number): Observable<TemporalPage> {
                return that.getLayer(metric).getHour(fromTS).expand(tpage => {
                    let url = tpage.link.next;
                    if (tpage.link.self.endsWith(Util.Time.getHourStr(toTS))) {
                        return Observable.empty();
                    } else {
                        return _utils.getAsTemporalPage(that.host + url, that.ax);
                    }
                });
            },
            getHourRecap(fromTS: number, toTS: number): Observable<NamedData> {
                return that.getLayer(metric).getHourRange(fromTS, toTS)
                    .map(o => o.data as NamedData)
                    .reduce((acc, curr) => _utils.mergeDataLayerData(acc, curr));
            },
            getHourRecapOf(...hourTS: number[]): Observable<NamedData> {
                return Observable.from(hourTS)
                    .flatMap(ts => that.getLayer(metric).getHour(ts))
                    .filter(o => !o.data.nodata)
                    .map(o => o.data as NamedData)
                    .reduce((acc, curr) => _utils.mergeDataLayerData(acc, curr));
            },
            getDay(dayTS: number): Observable<TemporalPage> {
                const day = Util.Time.getDayStr(dayTS);
                let url = _url(that.host, 'layers', that.cityId, metric, day);
                return _utils.getAsTemporalPage(url, that.ax);
            },
            getDayRange(fromTS: number, toTS: number): Observable<TemporalPage> {
                return that.getLayer(metric).getDay(fromTS).expand(tpage => {
                    let url = tpage.link.next;
                    if (tpage.link.self.endsWith(Util.Time.getDayStr(toTS))) {
                        return Observable.empty();
                    } else {
                        return _utils.getAsTemporalPage(that.host + url, that.ax);
                    }
                });
            },
            getDayRecap(fromTS: number, toTS: number): Observable<NamedData> {
                return that.getLayer(metric).getDayRange(fromTS, toTS)
                    .map(o => o.data as NamedData)
                    .reduce((acc, curr) => _utils.mergeDataLayerData(acc, curr));
            },
            getDayRecapOf(...dayTS: number[]): Observable<NamedData> {
                return Observable.from(dayTS)
                    .flatMap(ts => that.getLayer(metric).getDay(ts))
                    .map(o => o.data as NamedData)
                    .reduce((acc, curr) => _utils.mergeDataLayerData(acc, curr));
            },
        };
    }
}

/**
 * Methods for retrieving data Layers
 */
export interface IDataLayer {
    /** 
     * Returns data for the hour in which the timestamp falls.
     * @param hourTS The UTC timestamp of single hour
     * @see Util.Time.timestampOf to easily create a UTC timestamp
     */
    getHour: (hourTS: number) => Observable<TemporalPage>;

    /** 
     * Returns data for a range of hours, as requested.
     * @param fromTS The UTC timestamp of a single hour to start the range from 
     * @param toTS The UTC timestamp of a single hour to end the range with 
     * @see Util.Time.timestampOf to easily create a UTC timestamp
     */
    getHourRange: (fromTS: number, toTS: number) => Observable<TemporalPage>;

    /** 
     * Returns data for a recap of the given range of hours. 
     * A recap treats the total range as if it were one hour (weighted mean).
     * @param fromTS The UTC timestamp of a single hour to start the range from 
     * @param toTS The UTC timestamp of a single hour to end the range with 
     * @see Util.Time.timestampOf to easily create a UTC timestamp
     */
    getHourRecap: (fromTS: number, toTS: number) => Observable<NamedData>;

    /** 
     * Returns data for a recap of the given hour timestamps. 
     * A recap treats the total range as if it were one hour (weighted mean).
     * @param hourTS All timestamps representing the hours to get a recap from
     * @see Util.Time.timestampOf to easily create a UTC timestamp
     */
    getHourRecapOf: (...hourTS: number[]) => Observable<NamedData>;

    /** 
     * Returns data for the day in which the timestamp falls.
     * @param dayTS The UTC timestamp of single day
     * @see Util.Time.timestampOf to easily create a UTC timestamp
     */
    getDay: (dayTS: number) => Observable<TemporalPage>;

    /** 
     * Returns data for a range of days, as requested.
     * @param fromTS The UTC timestamp of a single day to start the range from 
     * @param toTS The UTC timestamp of a single day to end the range with 
     * @see Util.Time.timestampOf to easily create a UTC timestamp
     */
    getDayRange: (fromTS: number, toTS: number) => Observable<TemporalPage>;

    /** 
     * Returns data for a recap of the given range of days. 
     * A recap treats the total range as if it were one dat (weighted mean).
     * @param fromTS The UTC timestamp of a single day to start the range from 
     * @param toTS The UTC timestamp of a single day to end the range with 
     * @see Util.Time.timestampOf to easily create a UTC timestamp
     */
    getDayRecap: (fromTS: number, toTS: number) => Observable<NamedData>;

    /** 
    * Returns data for a recap of the given day timestamps. 
    * A recap treats the total range as if it were one day (weighted mean).
    * @param dayTS All timestamps representing the days to get a recap from
    * @see Util.Time.timestampOf to easily create a UTC timestamp
    */
    getDayRecapOf: (...dayTS: number[]) => Observable<NamedData>;
}
