import { Observable } from 'rxjs/Rx';
import axios from 'axios';
import { AxiosResponse, AxiosInstance } from 'axios';

import { InternalUtils as _utils } from '../internal-utils';
import { DataClient } from './data-client';

export class LocationsClient extends DataClient {
    private geohashes: string[];

    /**
     * Creates a new LocationsClient, it holds all the geohashes to fire simeltaneous requests to.
     * @see CotClient#createLocationsClient
     * @param host The host url of the new City of things client. (e.g. http://www.cot-backend.com/api);
     * @param axiosInstance {AxiosInstance} Custom axios instance to perform requests with, if null the defaul axios.create() is used.
     * @param geohashes The geohashes to consider
     */
    constructor(host: string, axiosInstance: AxiosInstance, ...geohashes: string[]) {
        super(host, axiosInstance || axios.create());
        this.geohashes = geohashes;
    }

    protected getMultiItems(): string[] {
        return this.geohashes;
    }
    
    protected getBaseApiPath(item: string): string {
        return '/locations/'+item;
    }

}