import { Observable } from 'rxjs/Rx';
import axios from 'axios';
import { AxiosResponse, AxiosInstance } from 'axios';

import { InternalUtils as _utils } from '../internal-utils';
import { DataClient } from './data-client';

export class SourcesClient extends DataClient {
    private sourceIds: string[];

    /**
     * Creates a new SourcesClient, it holds all the sourceIds to fire simeltaneous requests to.
     * @see CotClient#createSourcesClient
     * @param host The host url of the new City of things client. (e.g. http://www.cot-backend.com/api);
     * @param axiosInstance {AxiosInstance} Custom axios instance to perform requests with, if null the defaul axios.create() is used.
     * @param sourceIds The sourceIds to consider
     */
    constructor(host: string, axiosInstance: AxiosInstance, ...sourceIds: string[]) {
        super(host, axiosInstance || axios.create());
        this.sourceIds = sourceIds;
    }

    protected getMultiItems(): string[] {
        return this.sourceIds;
    }
    
    protected getBaseApiPath(item: string): string {
        return '/sources/'+item;
    }

}