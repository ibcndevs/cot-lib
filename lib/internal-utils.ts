import axios from 'axios';
import { AxiosResponse, AxiosInstance } from 'axios';
import { Observable } from 'rxjs/Rx';
import { TemporalPage, TemporalPageLink, NoData, Data, NamedData, BatchedData, STAT, STATDL } from './types';
import { Util } from './util';

export const enum TemporalPageData {
    EMPTY, BUCKET, STATS_UNIT, STATS_BATCH
};



const DEBUG = false;

export class InternalUtils {
    // const regex = new RegExp('^http(?:s)?\:\/\/[^\/]+', 'g');

    static getDataType(link: TemporalPageLink): TemporalPageData {
        if (link.self.indexOf('/stats/unit/') > -1) {
            return TemporalPageData.STATS_UNIT;
        }
        else if (link.self.indexOf('/stats/batch/') > -1) {
            return TemporalPageData.STATS_BATCH;
        }
        else if (link.self.indexOf('/event') > -1) {
            return TemporalPageData.BUCKET;
        }
        else {
            //CatchAll
            return TemporalPageData.BUCKET;
        }
    }

    /**
     * Mapping function that takes a response and maps it to a *TemporalPage*.
     * @param response The response to map.
     * @param readUntil {number=} Until which timestamp we already read, so up untill (inclusive) that one, will be filtered out.
     * @throws Error when no link header is present.
     * @returns A *TemporalPage* object containing the **data** field and the **link** field.
     */
    static mapToTemporalPage(response: AxiosResponse, readUntil?: number): TemporalPage {
        // new
        if (!response) {
            throw new Error('No Response present..');
        }
        // <-

        let link = this.parseLinkHeader(response.headers.link);
        let type = this.getDataType(link);

        if (response.headers.link) {
            // If 404 set type to NoData!
            if (response.status === 404) {
                type = TemporalPageData.EMPTY;
            }
            let data: Data | NamedData | BatchedData | NoData;
            switch (type) {
                case TemporalPageData.EMPTY:
                    data = { nodata: true };
                    // add possible status
                    data.status = response.status;
                    // add possible message
                    if (response.data) {
                        data.msg = response.data;
                    }
                    data = data as NoData;
                    break;
                case TemporalPageData.STATS_UNIT:
                case TemporalPageData.STATS_BATCH:
                    data = response.data as Data;
                    let vol = data.values && data.values.length > 0;
                    Object.assign(data, { nodata: !vol});
                    if (!vol) {
                        Object.assign(data, { values: []});
                    }
                    break;

                default:
                case TemporalPageData.BUCKET:
                    data = response.data as NamedData;
                    // If there is actual data and we have a readUntil value
                    if (!!readUntil && data.values && data.values.length > 0) {
                        // If first value is not bigger than what we read, then filter from where we passed our readUntil point.
                        if (data.values[0][0] <= readUntil) {
                            // console.log('filtering all bigger than '+readUntil);
                            data.values = data.values.filter((record: any[]) => record[0] > readUntil);
                        }
                    }
                    let filled = data.values && data.values.length > 0;
                    Object.assign(data, { nodata: !filled});
                    if (!filled) {
                        Object.assign(data, { values: []});
                    }

                    break;
            }
            return { data: data, link: link };
        }
        else {
            throw new Error('No link header present! (It should be there and at least contain a self link..)');
        }
    }

    /**
     * Parses the value of the link header, to make a *TemporalPageLink* object of the relevant information in there.
     * @param value The value of the link header to be parsed
     * @returns A *TemporalPageLink* object, containing at least a **self** link.
     */
    static parseLinkHeader(value: string): TemporalPageLink {
        let links: any = {};
        value.split(',').forEach(line => {
            let parts = line.split(';');
            let uri = parts[0].trim().slice(1, -1);
            let amount = parts.length;
            if (amount > 1) {
                for (let i = 1; i < amount; i++) {
                    let part = parts[i].split('=');
                    let key = part[0];
                    let value = part[1];
                    if ('rel' === key.trim()) {
                        links[value.trim().slice(1, -1)] = uri;
                    }
                    else {
                        console.log(key.trim() + ' => ' + value.trim());
                    }
                }
            }
        });
        return links;
    }

    /**
     * Perform a GET request to the url and map the result to a TemporalPage object.
     * @param url {string} The FULL url to GET query
     * @param axiosInstance {AxiosInstance} Custom axios instance to perform 
     * requests with, else the defaul axios.create() is used.
     * @param readUntil {number=} Until which timestamp we already read, so up untill (inclusive) that one, will be filtered out.
     */
    static getAsTemporalPage(url: string, axiosInstance: AxiosInstance, readUntil?: number): Observable<TemporalPage> {
        // let ax = axiosInstance || axios.create();
        return Observable.fromPromise(axiosInstance.get(url))
            .map(resp => InternalUtils.mapToTemporalPage(resp, readUntil));
            // .catch(err => Observable.of(InternalUtils.mapToTemporalPage(err.response, readUntil)));
    }

    /**
     * Recursively follows links to get the complete interval as specified in the url
     * 
     * @param host {string} The actual host endpoint as given to a Client instance
     * @param url {string} Url containing a *from* and *to* parameter
     * @param axiosInstance {AxiosInstance=} Optional custom axios instance to perform 
     * requests with, else the defaul axios.create() is used.
     */
    static getCompleteInterval(host: string, url: string, axiosInstance?: AxiosInstance): Observable<TemporalPage> {
        if (url.indexOf('from=') < 0 || url.indexOf('to=') < 0) {
            throw new Error('The url should contain a from and to query parameter for this method to work.');
        }
        if (host.endsWith('/')) {
            host = host.substring(0, host.length - 1);
        }
        let last: string = '';
        return this.getAsTemporalPage(url, axiosInstance)
            .do(tpage => last = tpage.link['last'])
            .expand(res => this.expandTest(res, host, last, axiosInstance));

    }

    /**
     * Returns the last timestamp that has been returned in the TemporalPage or null if there was no data.
     */
    static getLastTimestamp(tpage: TemporalPage): number | null {
        let bucket = tpage.data as NamedData;
        let t = (bucket && bucket.values && bucket.values.length > 0) ? bucket.values[bucket.values.length - 1][0] as number : null;
        return t;
    }

    /**
     * 
     * @param b1 
     * @param b2 
     */
    static mergeStatsBatchedData(b1: BatchedData, b2: BatchedData): BatchedData {
        const bins = b1.bins;
        let values: any[] = [];
        for (let i = 0; i < bins; i++) {
            const row1 = b1.values[i];
            const row2 = b2.values[i];
            values.push(this.unifyStatsRecord(row1, row2));
        }
        return {
            nodata: false,
            columns: b1.columns,
            values: values,
            bins: bins
        };
    }

    /** 
     * Merges 2 stat Data object.
     * @param d1 Data 1
     * @param d2 Data 2
     * @return A new Data containing the merged results
     */
    static mergeStatsData(d1: Data, d2: Data): Data {
        // Take first and only rows
        const row1 = d1.values[0];
        const row2 = d2.values[0];
        return {
            nodata: false,
            columns: d1.columns,
            values: [this.unifyStatsRecord(row1, row2)]
        };
    }

    /**
     * Merges two DataLayer Buckets.
     * @param b1 Bucket 1
     * @param b2 Bucket 2
     * @return A new DataLayer Bucket with merged data
     */
    static mergeDataLayerData(b1: NamedData, b2: NamedData): NamedData {
        let results: any[] = [];
        let handled: string[] = [];
        // all hashes from b1
        b1.values.forEach(row => {
            let idx = b2.values.findIndex(r => r[STATDL.HASH] === row[STATDL.HASH]);
            results.push(this.unifyDataLayerRecord(row, idx < 0 ? null : b2.values[idx]))
            handled.push(row[STATDL.HASH]);
        });
        // leftover hashes from b2
        b2.values
            .filter(row => handled.indexOf(row[STATDL.HASH]) < 0) // not yet handled
            .forEach(row => {
                let idx = b1.values.findIndex(r => r[STATDL.HASH] === row[STATDL.HASH]);
                results.push(this.unifyDataLayerRecord(row, idx < 0 ? null : b1.values[idx]))
            });
        return {
            nodata: false,
            name: b1.name,
            columns: b1.columns,
            values: results
        };
    }

    /**
     * Merge one stats record with one other record (record == row from Data.values)
     * @param r1 Record 1
     * @param r2 Record 2
     * @return A new record with merged data
     */
    private static unifyStatsRecord(r1: number[], r2: number[]): number[] {
        let result: number[] = [];
        if (r1[0] == null) {
            result.push(...r2);
        } else if (r2[0] == null) {
            result.push(...r1);
        } else {
            const count1 = r1[STAT.COUNT];
            const count2 = r2[STAT.COUNT];
            // Mean
            result[STAT.MEAN] = ((count1 * r1[STAT.MEAN] + count2 * r2[STAT.MEAN]) / (count1 + count2));
            // Min
            result[STAT.MIN] = Math.min(r1[STAT.MIN], r2[STAT.MIN]);
            // Max
            result[STAT.MAX] = Math.max(r1[STAT.MAX], r2[STAT.MAX]);
            // StdDev
            const std1 = r1[STAT.STDDEV];
            const std2 = r2[STAT.STDDEV];
            result[STAT.STDDEV] = Math.sqrt((count1 * std1 * std1 + count2 * std2 * std2) / (count1 + count2));
            result[STAT.COUNT] = count1 + count2;
        }
        return result;
    }

    /**
     * Merge one DataLayer record with one other record (record == row from Data.values)
     * @param r1 Record 1
     * @param r2 Record 2
     * @return A new record with merged data
     */
    private static unifyDataLayerRecord(r1: number[], r2: number[]): number[] {
        let result: number[] = [];
        if (r1 == null || r1.length === 0) {
            result.push(...r2);
        } else if (r2 == null || r2.length === 0) {
            result.push(...r1);
        } else {
            result[STATDL.HASH] = r1[STATDL.HASH];
            const count1 = r1[STATDL.COUNT];
            const count2 = r2[STATDL.COUNT];
            // Mean
            result[STATDL.MEAN] = ((count1 * r1[STATDL.MEAN] + count2 * r2[STATDL.MEAN]) / (count1 + count2));
            // Min
            result[STATDL.MIN] = Math.min(r1[STATDL.MIN], r2[STATDL.MIN]);
            // Max
            result[STATDL.MAX] = Math.max(r1[STATDL.MAX], r2[STATDL.MAX]);
            // StdDev
            const std1 = r1[STATDL.STDDEV];
            const std2 = r2[STATDL.STDDEV];
            result[STATDL.STDDEV] = Math.sqrt((count1 * std1 * std1 + count2 * std2 * std2) / (count1 + count2));
            result[STATDL.COUNT] = count1 + count2;
        }
        return result;
    }

    /**
     * The test that picks how to expand the initial result of a query.
     * @param tpage TemporalPage incoming object
     * @param last The Link header *last* field
     * @param axiosInstance {AxiosInstance=} Optional custom axios instance to perform 
     * requests with, else the defaul axios.create() is used.
     */
    private static expandTest(tpage: TemporalPage, host: string, last: string, axiosInstance?: AxiosInstance): Observable<TemporalPage> {
        if (!last) {
            if (DEBUG) { console.log('NO LAST: STOP'); }
            // no last, stop expanding
            return Observable.empty();
        }

        // last: complete last link path + querystring if any
        // myLast: path with out querystring
        let myLast = last.indexOf('?') > -1 ? last.split('?')[0] : last;

        // lastTo: timestamp *to* of querystring
        let lastTo: any;
        if (last.indexOf('to=') > -1) {
            lastTo = last.substring(last.indexOf('to=') + 3);
        }

        // case: last path == next link ::=> continue
        if (myLast === tpage.link['next']) {
            // last equals next link
            if (DEBUG) { console.log('last PATH (not ?querystr) equals *next*: GET LAST TPAGE'); }
            return this.getAsTemporalPage(host + last, axiosInstance);
        }
        // case: exact last link (+querystr) == exact self link (+querystr) ::=> stop
        else if (last === tpage.link['self']) {
            // this was the last request
            if (DEBUG) { console.log('*last* (with ?querystr) equals *self*: STOP'); }
            return Observable.empty();
        }
        // case: last path == self link ::=> stop
        else if (myLast === tpage.link['self']) {
            if (DEBUG) { console.log('*last* (without ?querystr) equals *self*: STOP --range in same bucket use case'); }
            return Observable.empty();
        }
        // case: last timestamp == from timestamp of self link ::=> stop
        else if (lastTo === tpage.link['self'].substr(tpage.link['self'].indexOf('from=') + 5)) {
            if (DEBUG) { console.log('last *?to=* (from ?querystr) equals *self?from=*: STOP ENDLESS LOOP IN RANGE CASE WITHIN BUCKET'); }
            return Observable.empty();
        }
        // case: default catch all ::=> continue
        else {
            // last didn't match next yet
            if (DEBUG) { console.log('no matches: GET *next* TPAGE'); }
            return this.getAsTemporalPage(host + tpage.link['next'], axiosInstance);
        }
    }
}

export function join(delim: string, ...parts: string[]): string {
    return parts.join(delim);
}

export function url(...parts: string[]): string {
    return join('/', ...parts);
}