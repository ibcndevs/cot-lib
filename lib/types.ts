export interface Type {
    id: string;
    metrics: string[];
}

export interface ExpandedType {
    id: string;
    metrics: Metric[];
}

export interface Metric {
    id: string;
    description: string;
    granularity: 'DAILY' | 'HOURLY';
    type: string;
}

export interface Source {
    id: string;
    name: string;
    typeRefs: string[];
    metrics: string[];
}

export interface ExpandedSource {
    id: string;
    name: string;
    typeRefs: ExpandedType[];
    metrics: Metric[];
}

export interface TemporalPage {
    data: Data | NamedData | BatchedData | NoData;
    link: TemporalPageLink;
}

export interface TemporalPageLink {
    self: string;
    next?: string;
    prev?: string;
    last?: string;
}



/** Statistics indexed for Stats Batch and Data */
export const enum STAT {
    MEAN,
    MIN,
    MAX,
    STDDEV,
    COUNT
}

/** Statistics indexes for DataLayer Buckets */
export const enum STATDL {
    HASH,
    MEAN,
    MIN,
    MAX,
    STDDEV,
    COUNT
};


/**
 * Also used as what's known as StatsUnit in the documentation.
 */
export interface Data {
    columns: string[];
    values: any[][];
    nodata: false;
}


export interface NamedData extends Data {
    name: string;
}

export interface BatchedData extends Data {
    bins: number;
}

export interface NoData {
    nodata: true;
    msg?: string;
    status?: number;
}
