import { Observable } from 'rxjs/Observable';
import { TemporalPage } from './types';
import * as ngeohash from 'ngeohash';

/**
 * Util namespace with several helper functions grouped in category classes
 */
export namespace Util {
    /**
     * Geohash functions.
     */
    export class Geohash {
        /**
     * Encodes a latitude-longitude point as a geohash with the given precision.
     * @param lat Latitude of point
     * @param lng Longitude of point
     * @param precision Charcter length of geohash *(defaults to 9)*
     */
        static encode(lat: number, lng: number, precision?: number): string {
            let prec = precision ? precision : 9;
            prec = Math.max(prec, 6);
            prec = Math.min(prec, 9);
            return ngeohash.encode(lat, lng, prec);
        }

        /**
         * Decode a given geohash as a latitude-longitude point.
         * @param geohash The geohash to decode
         */
        static decode(geohash: string): { "latitude": number, "longitude": number } {
            return ngeohash.decode(geohash);
        }

        /**
         * Get bounds of geohash
         * @param geohash The geohash to decode to bounds
         */
        static bounds(geohash: string): any {
            return ngeohash.decode_bbox(geohash);
        }

        /**
         * Is the given geohash (or its upperleftcorner) contained within the given coordinates
         * @param geohash 
         * @param minlat 
         * @param maxlat 
         * @param minlng 
         * @param maxlng 
         */
        static isContained(geohash: string, minlat: number, maxlat: number, minlng: number, maxlng: number): boolean {
            let loc = Util.Geohash.decode(geohash);
            return (minlat <= loc.latitude && loc.latitude <= maxlat)
                && (minlng <= loc.longitude && loc.longitude <= maxlng);
        }
    }

    /**
     * Time & data based functions.
     */
    export class Time {
        /**
         * Returns the unix (UTC) timestamp for the given date parameters. The arguments are considered to be in UTC time.
         * @param {number} year Four digit number
         * @param {number} month The month in the year (1-12)
         * @param {number=} day  The day of the month (1-31)
         * @param {number=} hours The hour of the day (0-23)
         * @param {number=} minutes The minute of the day (0-59)
         * @param {number=} seconds The seconds of the day (0-59)
         * @param {number=} milliseconds The milliseconds of the day (0-999)
         */
        static timestamp(year: number, month: number, day = 1, hours = 0, minutes = 0, seconds = 0, milliseconds = 0): number {
            return Date.UTC(year, month - 1, day, hours, minutes, seconds, milliseconds);
        }

        /**
         * Returns the string representation that can be used in the REST api urls.
         * (eg. 20171205 for December 5, 2017)
         * @param timestamp The UTC timestamp to convert
         */
        static getDayStr(timestamp: number): string {
            let d = new Date(timestamp);
            let offset = d.getTimezoneOffset();
            d.setMinutes(d.getMinutes()-offset);
            let m: any = d.getUTCMonth() + 1;
            m = (m < 10) ? '0' + m : m;
            let dd: any = d.getUTCDate();
            dd = (dd < 10) ? '0' + dd : dd;
            return '' + d.getUTCFullYear() + m + dd;
        }

        /**
         * Returns the string representation that can be used in the REST api urls.
         * (eg. 20171205/13 for December 5, 2017 at 1pm)
         * @param timestamp The UTC timestamp to convert
         */
        static getHourStr(timestamp: number): string {
            let d = new Date(timestamp);
            let h: any = d.getUTCHours();
            return this.getDayStr(timestamp) + '/' + h;
        }

        /**
         * Returns an array of 7 string arrays (mon-sun). Each array contains the UTC timestamps for each instance of that weekday in the month.
         * @param month The month (1-12)
         */
        static getWeekdaysTS(year: number, month: number): number[][] {
            //  Month not minus 1, since need to ask next month, but day 0
            let date = new Date(Date.UTC(year, month, 0));
            let days = date.getDate();
            let result: any[] = [];
            for (let i = 0; i < days; i++) {
                let wd = new Date(Date.UTC(year, month - 1, i + 1));
                let idx = (7 + wd.getUTCDay() - 1) % 7;
                if (idx in result) {
                    result[idx].push(wd.getTime());
                } else {
                    result[idx] = [wd.getTime()];
                }
            }
            return result;
        }
    }
}